//alert("Hi!")
//disabled button is blocking the update button in web in the html.js
/*
	Syntax: 
		fetch(url, options)

*/
//Get post data using fetch
//fetch is get by default
fetch('https://jsonplaceholder.typicode.com/posts') //can put the link given by heroku on api, can use the endpoint in server/api
.then((response) => response.json()) //response will be converted to json file
.then((data) => showPosts(data)); //will print in the console the json data/file or also use console.log(data)

//Add post data


document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		//method is what will be the request
		//json is to send json data in web
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: { 'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {

		console.log(data)
		alert('Successfully added.');

		document.querySelector('#txt-title').value = null;//make value null to clear the value
		document.querySelector('#txt-body').value = null;

	})

})


//Show posts
const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Edit post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');

}

//Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: { 'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Successfully Updated')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

/*
	Mini Activity:
		Retrieve/Get a single post. Print in the console
*/


//retrieve a particular object
fetch('https://jsonplaceholder.typicode.com/posts/2') //can put the link given by heroku on api, can use the endpoint in server/api
.then((response) => response.json()) //response will be converted to json file
.then((data) => console.log(data)); //will print in the console the json data/file or also use console.log(data)

//delete
const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove();
}
